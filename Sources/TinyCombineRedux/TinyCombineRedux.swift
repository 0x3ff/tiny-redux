//
//  TinyRedux.swift
//  TinyRedux
//
//  Created by Андрей Трифонов on 04.11.2022.
//

import Combine
import Foundation


public protocol Reducer<State, Action> {
    associatedtype State
    associatedtype Action

    func reduce(oldState: State, withAction: Action) -> State
}

public protocol Middleware<Action> {
    associatedtype Action

    func handle(action: Action) -> AnyPublisher<Action, Never>?
}

public class Store<State, Action, Scheduler: Combine.Scheduler>: ObservableObject {
    @Published public private(set) var state: State

    private let input = PassthroughSubject<Action, Never>()
    private var cancellable = Set<AnyCancellable>()

    public init(initialState: State,
                reducer: some Reducer<State, Action>,
                middlewares: some Collection<any Middleware<Action>>,
                scheduler: Scheduler) {
        self.state = initialState

        let events = middlewares.map { [unowned self] mw in
            return input
                .receive(on: scheduler)
                .map { mw.handle(action: $0) }
                .compactMap { $0 }
                .switchToLatest()
                .eraseToAnyPublisher()
        }

        Publishers.MergeMany(events)
            .receive(on: scheduler)
            .sink { [unowned self] in dispatch($0) }
            .store(in: &cancellable)

        input
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] in state = reducer.reduce(oldState: state, withAction: $0) }
            .store(in: &cancellable)
    }

    public func dispatch(_ action: Action) {
        input.send(action)
    }
}
