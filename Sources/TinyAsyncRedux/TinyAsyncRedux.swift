//
//  File.swift
//  
//
//  Created by Андрей Трифонов on 2023-01-13.
//

import Foundation


public protocol Reducer<State, Action> {
    associatedtype State
    associatedtype Action

    func reduce(state: State, withAction action: Action) -> State
}

public protocol Middleware<Action> {
    associatedtype Action

    func handle(action: Action) -> AsyncStream<Action>?
}

@MainActor public class Store<State, Action>: ObservableObject {
    @Published public private(set) var state: State

    private let reducer: any Reducer<State, Action>
    private let middlewares: [any Middleware<Action>]
    private var tasks: [Task<Void, Never>?]

    public init(initialState: State,
                reducer: some Reducer<State, Action>,
                middlewares: [any Middleware<Action>]
    ) {
        self.state = initialState
        self.reducer = reducer
        self.middlewares = middlewares
        self.tasks = Array(repeating: nil, count: middlewares.count)
    }

    deinit {
        tasks.forEach { $0?.cancel() }
    }

    public func send(_ action: Action) {
        state = reducer.reduce(state: state, withAction: action)

        for (i, m) in middlewares.enumerated() {
            guard let actions = m.handle(action: action) else {
                continue
            }

            tasks[i]?.cancel()

            tasks[i] = Task { [weak self] in
                for await a in actions {
                    self?.send(a)
                }
            }
        }
    }
}
