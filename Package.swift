// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription


let package = Package(
    name: "TinyRedux",
    platforms: [
        .iOS(.v16),
        .tvOS(.v16),
        .watchOS(.v9),
        .macOS(.v13)
      ],
    products: [
        .library(
            name: "TinyCombineRedux",
            targets: ["TinyCombineRedux"]),
        .library(
            name: "TinyAsyncRedux",
            targets: ["TinyAsyncRedux"])
    ],
    targets: [
        .target(
            name: "TinyCombineRedux"),
        .testTarget(
            name: "TinyCombineReduxTests",
            dependencies: ["TinyCombineRedux"]),
        .target(
            name: "TinyAsyncRedux"),
        .testTarget(
            name: "TinyAsyncReduxTests",
            dependencies: ["TinyAsyncRedux"])
    ]
)
