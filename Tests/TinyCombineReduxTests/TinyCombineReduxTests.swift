import Combine
import XCTest

@testable import TinyCombineRedux


enum AppAction {
    case actionOne
    case actionTwo
    case actionThree
    case actionFour
}

struct AppState: Equatable {
    var value: Int
}

struct AppReducer: Reducer {
    private let closure: (AppState, AppAction) -> AppState

    init(closure: @escaping (AppState, AppAction) -> AppState = { s, _ in s}) {
        self.closure = closure
    }

    func reduce(oldState state: AppState, withAction action: AppAction) -> AppState {
        closure(state, action)
    }
}

struct AppMiddleware: Middleware {
    private let closure: (AppAction) -> AnyPublisher<AppAction, Never>?

    init(closure: @escaping (AppAction) -> AnyPublisher<AppAction, Never>?) {
        self.closure = closure
    }

    func handle(action: AppAction) -> AnyPublisher<AppAction, Never>? {
        closure(action)
    }
}

final class TinyCombineReduxTests: XCTestCase {
    let initialValue = 0
    let newValue = 1

    let actionOne = AppAction.actionOne
    let actionTwo = AppAction.actionTwo

    var expectationOne: XCTestExpectation!
    var expectationTwo: XCTestExpectation!
    var invertedExpectation: XCTestExpectation!

    var bag = Set<AnyCancellable>()

    override func setUp() async throws {
        expectationOne = XCTestExpectation()
        expectationOne.expectedFulfillmentCount = 1
        expectationOne.assertForOverFulfill = true

        expectationTwo = XCTestExpectation()
        expectationTwo.expectedFulfillmentCount = 1
        expectationTwo.assertForOverFulfill = true

        invertedExpectation = XCTestExpectation()
        invertedExpectation.isInverted = true
    }

    override func tearDown() {
        bag.removeAll()
    }

    func testInitialState_IsSet() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.$state
            .receive(on: DispatchQueue.global(qos: .default))
            .sink { [unowned self] state in
                XCTAssertEqual(state.value, self.initialValue)
                expectationOne.fulfill()
            }
            .store(in: &bag)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testAction_IsPassedToReducer() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                XCTAssertEqual(action, actionOne)
                expectationOne.fulfill()
                return state
            },
            middlewares: [],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.dispatch(actionOne)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testAction_IsPassedToMiddleware() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [ AppMiddleware(closure: { [unowned self] action in
                if action == actionOne {
                    expectationOne.fulfill()
                }
                return nil
            })],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.dispatch(actionOne)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testReducer_ChangesState() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, _ in
                var newState = state
                newState.value = newValue
                return newState
            },
            middlewares: [],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.dispatch(actionOne)
        store.$state
            .receive(on: DispatchQueue.global(qos: .default))
            .sink { [unowned self] state in
                if state.value == newValue {
                    expectationOne.fulfill()
                }
            }
            .store(in: &bag)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testMiddlewareAction_IsPassedToReducer() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                if action == actionTwo {
                    expectationOne.fulfill()
                }
                return state
            },
            middlewares: [ AppMiddleware(closure: { [unowned self] action in
                if action == actionOne {
                    return Just(actionTwo).eraseToAnyPublisher()
                }
                return nil
            })],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.dispatch(actionOne)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testMiddlewareAction_NewRewritesOld() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [ AppMiddleware(closure: { [unowned self] action in
                switch action {
                case .actionOne:
                    return Future { promise in
                        DispatchQueue.global(qos: .default).async {
                            sleep(1)
                            promise(.success(.actionThree))
                        }
                    }.eraseToAnyPublisher()
                case .actionTwo:
                    return Future { promise in
                        DispatchQueue.global(qos: .default).async {
                            sleep(2)
                            promise(.success(.actionFour))
                        }
                    }.eraseToAnyPublisher()
                case .actionThree:
                    invertedExpectation.fulfill()
                case .actionFour:
                    expectationOne.fulfill()
                }
                return nil
            })],
            scheduler: DispatchQueue.global(qos: .default))

        store.dispatch(actionOne)
        store.dispatch(actionTwo)

        wait(for: [expectationOne, invertedExpectation], timeout: 5.0)
    }

    func testMiddlewareAction_NilNotRewritesOld() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [ AppMiddleware(closure: { [unowned self] action in
                switch action {
                case .actionOne:
                    return Future { promise in
                        DispatchQueue.global(qos: .default).async {
                            sleep(1)
                            promise(.success(.actionThree))
                        }
                    }.eraseToAnyPublisher()
                case .actionTwo:
                    expectationOne.fulfill()
                case .actionThree:
                    expectationTwo.fulfill()
                default:
                    break
                }
                return nil
            })],
            scheduler: DispatchQueue.global(qos: .default))

        store.dispatch(actionOne)
        store.dispatch(actionTwo)

        wait(for: [expectationOne, expectationTwo], timeout: 5.0, enforceOrder: true)
    }

    func testMiddlewareAction_IsPassedToReducerAfterNil() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                switch action {
                case .actionThree:
                    expectationTwo.fulfill()
                default:
                    break
                }
                return state },
            middlewares: [ AppMiddleware(closure: { [unowned self] action in
                switch action {
                case .actionOne:
                    expectationOne.fulfill()
                case .actionTwo:
                    return Just(.actionThree).eraseToAnyPublisher()
                default:
                    break
                }
                return nil
            })],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.dispatch(actionOne)
        store.dispatch(actionTwo)

        wait(for: [expectationOne, expectationTwo], timeout: 5.0, enforceOrder: true)
    }

    func testTwoMiddlewareAction_IsPassedToReducer() {
        let store = Store<AppState, AppAction, DispatchQueue>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                switch action {
                case .actionTwo:
                    expectationOne.fulfill()
                case .actionThree:
                    expectationTwo.fulfill()
                default:
                    break
                }
                return state },
            middlewares: [ AppMiddleware(closure: { action in
                switch action {
                case .actionOne:
                    return Just(.actionTwo).eraseToAnyPublisher()
                default:
                    return nil
                }
            }), AppMiddleware(closure: { action in
                switch action {
                case .actionOne:
                    return Just(.actionThree).eraseToAnyPublisher()
                default:
                    return nil
                }
            })],
            scheduler: DispatchQueue.global(qos: .default)
        )

        store.dispatch(actionOne)

        wait(for: [expectationOne, expectationTwo], timeout: 5.0)
    }
}
