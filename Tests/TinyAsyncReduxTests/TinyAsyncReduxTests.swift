import Combine
import XCTest

@testable import TinyAsyncRedux


enum AppAction {
    case actionOne
    case actionTwo
    case actionThree
    case actionFour
}

struct AppState: Equatable {
    var value: Int
}

struct AppMiddleware: Middleware {
    private let task: (AppAction) -> AsyncStream<AppAction>?

    init(task: @escaping (AppAction) -> AsyncStream<AppAction>?) {
        self.task = task
    }

    func handle(action: AppAction) -> AsyncStream<AppAction>? {
        return task(action)
    }
}

struct AppReducer: Reducer {
    private let task: (AppState, AppAction) -> AppState

    init(task: @escaping (AppState, AppAction) -> AppState = { s, _ in s }) {
        self.task = task
    }
    func reduce(state: AppState, withAction action: AppAction) -> AppState {
        return task(state, action)
    }
}

@MainActor final class TinyAsyncReduxTests: XCTestCase {
    let initialValue = 0
    let newValue = 1

    let actionOne = AppAction.actionOne
    let actionTwo = AppAction.actionTwo

    var expectationOne: XCTestExpectation!
    var expectationTwo: XCTestExpectation!
    var invertedExpectation: XCTestExpectation!

    var bag = Set<AnyCancellable>()

    override func setUp() async throws {
        expectationOne = XCTestExpectation()
        expectationOne.expectedFulfillmentCount = 1
        expectationOne.assertForOverFulfill = true

        expectationTwo = XCTestExpectation()
        expectationTwo.expectedFulfillmentCount = 1
        expectationTwo.assertForOverFulfill = true

        invertedExpectation = XCTestExpectation()
        invertedExpectation.isInverted = true
    }

    override func tearDown() {
        bag.removeAll()
    }

    func testInitialState_IsSet() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: []
        )

        store.$state
            .receive(on: DispatchQueue.global(qos: .default))
            .sink { [unowned self] state in
                XCTAssertEqual(state.value, self.initialValue)
                expectationOne.fulfill()
            }
            .store(in: &bag)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testAction_IsPassedToReducer() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                XCTAssertEqual(action, actionOne)
                expectationOne.fulfill()
                return state
            },
            middlewares: []
        )

        store.send(actionOne)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testAction_IsPassedToMiddleware() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [ AppMiddleware(task: { [unowned self] action in
                if action == actionOne {
                    expectationOne.fulfill()
                }
                return nil
            })]
        )

        store.send(actionOne)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testReducer_ChangesState() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, _ in
                var newState = state
                newState.value = newValue
                return newState
            },
            middlewares: []
        )

        store.send(actionOne)
        store.$state
            .receive(on: DispatchQueue.global(qos: .default))
            .sink { [unowned self] state in
                if state.value == newValue {
                    expectationOne.fulfill()
                }
            }
            .store(in: &bag)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testMiddlewareAction_IsPassedToReducer() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                if action == actionTwo {
                    expectationOne.fulfill()
                }
                return state
            },
            middlewares: [ AppMiddleware(task: { [unowned self] action in
                if action == actionOne {
                    return AsyncStream { .actionTwo }
                }
                return nil
            })]
        )

        store.send(actionOne)

        wait(for: [expectationOne], timeout: 5.0)
    }

    func testTwoMiddlewareAction_IsPassedToReducer() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                switch action {
                case .actionTwo:
                    expectationOne.fulfill()
                case .actionThree:
                    expectationTwo.fulfill()
                default:
                    break
                }
                return state },
            middlewares: [ AppMiddleware(task: { action in
                switch action {
                case .actionOne:
                    return AsyncStream { .actionTwo }
                default:
                    return nil
                }
            }), AppMiddleware(task: { action in
                switch action {
                case .actionOne:
                    return AsyncStream { .actionThree }
                default:
                    return nil
                }
            })]
        )

        store.send(.actionOne)

        wait(for: [expectationOne, expectationTwo], timeout: 5.0)
    }

    func testMiddlewareAction_NewRewritesOld() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [ AppMiddleware(task: { [unowned self] action in
                switch action {
                case .actionOne:
                    return AsyncStream { continuation in
                        DispatchQueue.global(qos: .default).async {
                            sleep(1)
                            continuation.yield(.actionThree)
                            continuation.finish()
                        }
                    }
                case .actionTwo:
                    return AsyncStream { continuation in
                        DispatchQueue.global(qos: .default).async {
                            sleep(2)
                            continuation.yield(.actionFour)
                            continuation.finish()
                        }
                    }
                case .actionThree:
                    invertedExpectation.fulfill()
                case .actionFour:
                    expectationOne.fulfill()
                }
                return nil
            })]
        )

        store.send(actionOne)
        store.send(actionTwo)

        wait(for: [expectationOne, invertedExpectation], timeout: 5.0)
    }

    func testMiddlewareAction_NilNotRewritesOld() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer(),
            middlewares: [ AppMiddleware(task: { [unowned self] action in
                switch action {
                case .actionOne:
                    return AsyncStream { continuation in
                        DispatchQueue.global(qos: .default).async {
                            sleep(1)
                            continuation.yield(.actionThree)
                            continuation.finish()
                        }
                    }
                case .actionTwo:
                    expectationOne.fulfill()
                case .actionThree:
                    expectationTwo.fulfill()
                default:
                    break
                }
                return nil
            })]
        )

        store.send(actionOne)
        store.send(actionTwo)

        wait(for: [expectationOne, expectationTwo], timeout: 5.0, enforceOrder: true)
    }

    func testMiddlewareAction_IsPassedToReducerAfterNil() {
        let store = Store<AppState, AppAction>(
            initialState: AppState(value: initialValue),
            reducer: AppReducer { [unowned self] state, action in
                switch action {
                case .actionThree:
                    expectationTwo.fulfill()
                default:
                    break
                }
                return state },
            middlewares: [ AppMiddleware(task: { [unowned self] action in
                switch action {
                case .actionOne:
                    expectationOne.fulfill()
                case .actionTwo:
                    return AsyncStream { .actionThree }
                default:
                    break
                }
                return nil
            })]
        )

        store.send(actionOne)
        store.send(actionTwo)

        wait(for: [expectationOne, expectationTwo], timeout: 5.0, enforceOrder: true)
    }
}
