# TinyRedux

[![CircleCI](https://dl.circleci.com/status-badge/img/circleci/H7qMxXd97VM66B8pBw7yNh/3iy3Mf9wNKr7Xxk1xVADNx/tree/main.svg?style=shield&circle-token=eee5efc7a07f5c1046ea48084c3be9425701c924)](https://dl.circleci.com/status-badge/redirect/circleci/H7qMxXd97VM66B8pBw7yNh/3iy3Mf9wNKr7Xxk1xVADNx/tree/main)
[![codecov](https://codecov.io/gl/0x3ff/tiny-redux/graph/badge.svg?token=0L09HMLENW)](https://codecov.io/gl/0x3ff/tiny-redux)

TinyRedux is a lightweight Redux-like core for iOS apps written in Swift.

## Libraries

TinyRedux contains two libraries:
- TyneAsyncRedux is the version based on Apple's async/await.
- TyneCombineRedux is the version based on Apple's Combine Framework.

Both of them provide a simple way to connect UI and business logic and send data between layers.
 
 


